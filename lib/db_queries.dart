import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:flutter_app/db_requests.dart';

class DBApi {
  Widget getConfig() {
    int iteration = 0;
    return Query(
      options: QueryOptions(
        document: appConfigRequest,
        // this is the query string you just created
        pollInterval: 15,
      ),
      // Just like in apollo refetch() could be used to manually trigger a refetch
      builder: (QueryResult result, {VoidCallback refetch}) {
        if (result.errors != null) {
          return Text(result.errors.toString());
        }

        if (result.loading) {
          iteration = iteration++;
          String out = 'Loading';
          for (int i = 0; i <= iteration; i++) {
            out += '.';
          }
          return Text(out);
        }

        // it can be either Map or List
        List payMethodTypes =
        result.data['appConfig']['supportedPaymentTypes'];

        return ListView.builder(
            itemCount: payMethodTypes.length,
            itemBuilder: (context, index) {
              String displayText = payMethodTypes[index];
              return Center(
                child: Text(displayText),
              );
            });
      },
    );
  }
}