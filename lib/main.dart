import 'package:flutter/material.dart';
import 'package:flutter_app/db_auth_link.dart';
import 'package:flutter_app/db_queries.dart';
import 'package:flutter_app/map.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final HttpLink httpLink = HttpLink(
      uri: 'https://dev.db.stuzo.net/graphql/',
    );

    final DBAuthLink authLink = DBAuthLink(
      getToken: () async => 'Bearer <YOUR_PERSONAL_ACCESS_TOKEN>',
      // OR
      // getToken: () => 'Bearer <YOUR_PERSONAL_ACCESS_TOKEN>',
    );

    final Link link = authLink.concat(httpLink as Link);

    ValueNotifier<GraphQLClient> client = ValueNotifier(
      GraphQLClient(
        cache: InMemoryCache(),
        link: link,
      ),
    );

    return GraphQLProvider(
      client: client,
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          // is not restarted.
          primarySwatch: Colors.blue,
        ),
        home: MyHomePage(title: 'Flutter Demo'),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Widget mainContent = Center(
    child: Text("Hello World"),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        // in the middle of the parent.
        child: mainContent,
      ),
      floatingActionButton: Wrap(
        direction: Axis.horizontal,
        spacing: 100,
        children: <Widget>[
          FloatingActionButton(
            heroTag: "btn1",
            onPressed: () {
              setState(() {
                mainContent = DBApi().getConfig();
              });
            },
            tooltip: 'Center Btn',
            child: Icon(Icons.adb),
          ),
          RawMaterialButton(
            fillColor: Colors.deepOrange,
            splashColor: Colors.orange,
            child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 15, horizontal: 50),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const Icon(
                      Icons.map,
                      color: Colors.white,
                    ),
                    const SizedBox(width: 5,),
                    const Text(
                      "Map",
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                )),
            shape: const StadiumBorder(),
            onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => MapPage()));
            },
          ),
//      FloatingActionButton(
//      heroTag: "btn2",
//      onPressed: () {
//
//      },
//      tooltip: 'End Btn',
//      child: Icon(Icons.map),
//    ),
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
