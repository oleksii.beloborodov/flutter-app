import 'dart:async';

// ignore: implementation_imports
import 'package:graphql/src/link/link.dart';
// ignore: implementation_imports
import 'package:graphql/src/link/operation.dart';
// ignore: implementation_imports
import 'package:graphql/src/link/fetch_result.dart';

typedef GetToken = FutureOr<String> Function();

class DBAuthLink extends Link {
  DBAuthLink({
    this.getToken,
  }) : super(
    request: (Operation operation, [NextLink forward]) {
      StreamController<FetchResult> controller;

      Future<void> onListen() async {
        try {
          final String token = await getToken();

          operation.setContext(<String, Map<String, String>>{
            'headers': <String, String>{
//              'Authorization': token,
              'x-api-key': 'S2SI5501q3SeKQR3ZLqpxcrVPuHUAxV0jCPCN8em8QRGoQyt'
            }
          });
        } catch (error) {
          controller.addError(error);
        }

        await controller.addStream(forward(operation));
        await controller.close();
      }

      controller = StreamController<FetchResult>(onListen: onListen);

      return controller.stream;
    },
  );

  GetToken getToken;
}